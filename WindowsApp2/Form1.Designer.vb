﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.LOrdernummer = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CSV = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.AanmakenCSV = New System.Windows.Forms.Button()
        Me.OpenFiles = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Clear = New System.Windows.Forms.Button()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OrdernummerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MateriaalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AantalminDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AantalMaxDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DikteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LokatieDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BestandsnaamDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WerknummerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CamLayerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.OrdernummerDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MateriaalDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AantalminDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AantalMaxDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DikteDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LokatieDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BestandsnaamDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WerknummerDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CamLayerDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PonsnibBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BrakelSuiteDbDataSet = New WindowsApp2.BrakelSuiteDbDataSet()
        Me.PonsnibTableAdapter = New WindowsApp2.BrakelSuiteDbDataSetTableAdapters.PonsnibTableAdapter()
        Me.BindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PonsnibBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BrakelSuiteDbDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ListBox1
        '
        Me.ListBox1.AllowDrop = True
        Me.ListBox1.BackColor = System.Drawing.Color.White
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(1, 1)
        Me.ListBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(408, 407)
        Me.ListBox1.TabIndex = 1
        '
        'LOrdernummer
        '
        Me.LOrdernummer.AutoSize = True
        Me.LOrdernummer.Location = New System.Drawing.Point(436, 15)
        Me.LOrdernummer.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LOrdernummer.Name = "LOrdernummer"
        Me.LOrdernummer.Size = New System.Drawing.Size(70, 13)
        Me.LOrdernummer.TabIndex = 2
        Me.LOrdernummer.Text = "Ordernummer"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(529, 15)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(89, 20)
        Me.TextBox1.TabIndex = 3
        Me.TextBox1.Text = "BC12345.123"
        '
        'CSV
        '
        Me.CSV.Location = New System.Drawing.Point(529, 60)
        Me.CSV.Margin = New System.Windows.Forms.Padding(2)
        Me.CSV.Name = "CSV"
        Me.CSV.Size = New System.Drawing.Size(89, 20)
        Me.CSV.TabIndex = 5
        Me.CSV.Text = "Zalf"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(436, 60)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Benaming CSV"
        '
        'AanmakenCSV
        '
        Me.AanmakenCSV.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.AanmakenCSV.Location = New System.Drawing.Point(529, 135)
        Me.AanmakenCSV.Margin = New System.Windows.Forms.Padding(2)
        Me.AanmakenCSV.Name = "AanmakenCSV"
        Me.AanmakenCSV.Size = New System.Drawing.Size(89, 21)
        Me.AanmakenCSV.TabIndex = 6
        Me.AanmakenCSV.Text = "Create  CSV"
        Me.AanmakenCSV.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.AanmakenCSV.UseVisualStyleBackColor = True
        '
        'OpenFiles
        '
        Me.OpenFiles.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.OpenFiles.Location = New System.Drawing.Point(529, 98)
        Me.OpenFiles.Margin = New System.Windows.Forms.Padding(2)
        Me.OpenFiles.Name = "OpenFiles"
        Me.OpenFiles.Size = New System.Drawing.Size(89, 21)
        Me.OpenFiles.TabIndex = 7
        Me.OpenFiles.Text = "Open Files"
        Me.OpenFiles.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.OpenFiles.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Button1.BackColor = System.Drawing.Color.Red
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.Button1.Location = New System.Drawing.Point(529, 387)
        Me.Button1.Margin = New System.Windows.Forms.Padding(2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(89, 21)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "End"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Clear
        '
        Me.Clear.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Clear.Location = New System.Drawing.Point(529, 177)
        Me.Clear.Margin = New System.Windows.Forms.Padding(2)
        Me.Clear.Name = "Clear"
        Me.Clear.Size = New System.Drawing.Size(89, 21)
        Me.Clear.TabIndex = 9
        Me.Clear.Text = "Clear"
        Me.Clear.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Clear.UseVisualStyleBackColor = True
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "Id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "Id"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        Me.IdDataGridViewTextBoxColumn.ReadOnly = True
        '
        'OrdernummerDataGridViewTextBoxColumn
        '
        Me.OrdernummerDataGridViewTextBoxColumn.DataPropertyName = "Ordernummer"
        Me.OrdernummerDataGridViewTextBoxColumn.HeaderText = "Ordernummer"
        Me.OrdernummerDataGridViewTextBoxColumn.Name = "OrdernummerDataGridViewTextBoxColumn"
        '
        'MateriaalDataGridViewTextBoxColumn
        '
        Me.MateriaalDataGridViewTextBoxColumn.DataPropertyName = "Materiaal"
        Me.MateriaalDataGridViewTextBoxColumn.HeaderText = "Materiaal"
        Me.MateriaalDataGridViewTextBoxColumn.Name = "MateriaalDataGridViewTextBoxColumn"
        '
        'AantalminDataGridViewTextBoxColumn
        '
        Me.AantalminDataGridViewTextBoxColumn.DataPropertyName = "Aantalmin"
        Me.AantalminDataGridViewTextBoxColumn.HeaderText = "Aantalmin"
        Me.AantalminDataGridViewTextBoxColumn.Name = "AantalminDataGridViewTextBoxColumn"
        '
        'AantalMaxDataGridViewTextBoxColumn
        '
        Me.AantalMaxDataGridViewTextBoxColumn.DataPropertyName = "AantalMax"
        Me.AantalMaxDataGridViewTextBoxColumn.HeaderText = "AantalMax"
        Me.AantalMaxDataGridViewTextBoxColumn.Name = "AantalMaxDataGridViewTextBoxColumn"
        '
        'DikteDataGridViewTextBoxColumn
        '
        Me.DikteDataGridViewTextBoxColumn.DataPropertyName = "Dikte"
        Me.DikteDataGridViewTextBoxColumn.HeaderText = "Dikte"
        Me.DikteDataGridViewTextBoxColumn.Name = "DikteDataGridViewTextBoxColumn"
        '
        'LokatieDataGridViewTextBoxColumn
        '
        Me.LokatieDataGridViewTextBoxColumn.DataPropertyName = "Lokatie"
        Me.LokatieDataGridViewTextBoxColumn.HeaderText = "Lokatie"
        Me.LokatieDataGridViewTextBoxColumn.Name = "LokatieDataGridViewTextBoxColumn"
        '
        'BestandsnaamDataGridViewTextBoxColumn
        '
        Me.BestandsnaamDataGridViewTextBoxColumn.DataPropertyName = "Bestandsnaam"
        Me.BestandsnaamDataGridViewTextBoxColumn.HeaderText = "Bestandsnaam"
        Me.BestandsnaamDataGridViewTextBoxColumn.Name = "BestandsnaamDataGridViewTextBoxColumn"
        '
        'WerknummerDataGridViewTextBoxColumn
        '
        Me.WerknummerDataGridViewTextBoxColumn.DataPropertyName = "Werknummer"
        Me.WerknummerDataGridViewTextBoxColumn.HeaderText = "Werknummer"
        Me.WerknummerDataGridViewTextBoxColumn.Name = "WerknummerDataGridViewTextBoxColumn"
        '
        'CamLayerDataGridViewTextBoxColumn
        '
        Me.CamLayerDataGridViewTextBoxColumn.DataPropertyName = "CamLayer"
        Me.CamLayerDataGridViewTextBoxColumn.HeaderText = "CamLayer"
        Me.CamLayerDataGridViewTextBoxColumn.Name = "CamLayerDataGridViewTextBoxColumn"
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OrdernummerDataGridViewTextBoxColumn1, Me.MateriaalDataGridViewTextBoxColumn1, Me.AantalminDataGridViewTextBoxColumn1, Me.AantalMaxDataGridViewTextBoxColumn1, Me.DikteDataGridViewTextBoxColumn1, Me.LokatieDataGridViewTextBoxColumn1, Me.BestandsnaamDataGridViewTextBoxColumn1, Me.WerknummerDataGridViewTextBoxColumn1, Me.CamLayerDataGridViewTextBoxColumn1})
        Me.DataGridView1.DataSource = Me.PonsnibBindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(12, 605)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(510, 408)
        Me.DataGridView1.TabIndex = 10
        '
        'OrdernummerDataGridViewTextBoxColumn1
        '
        Me.OrdernummerDataGridViewTextBoxColumn1.DataPropertyName = "Ordernummer"
        Me.OrdernummerDataGridViewTextBoxColumn1.HeaderText = "Ordernummer"
        Me.OrdernummerDataGridViewTextBoxColumn1.Name = "OrdernummerDataGridViewTextBoxColumn1"
        '
        'MateriaalDataGridViewTextBoxColumn1
        '
        Me.MateriaalDataGridViewTextBoxColumn1.DataPropertyName = "Materiaal"
        Me.MateriaalDataGridViewTextBoxColumn1.HeaderText = "Materiaal"
        Me.MateriaalDataGridViewTextBoxColumn1.Name = "MateriaalDataGridViewTextBoxColumn1"
        '
        'AantalminDataGridViewTextBoxColumn1
        '
        Me.AantalminDataGridViewTextBoxColumn1.DataPropertyName = "Aantalmin"
        Me.AantalminDataGridViewTextBoxColumn1.HeaderText = "Aantalmin"
        Me.AantalminDataGridViewTextBoxColumn1.Name = "AantalminDataGridViewTextBoxColumn1"
        '
        'AantalMaxDataGridViewTextBoxColumn1
        '
        Me.AantalMaxDataGridViewTextBoxColumn1.DataPropertyName = "AantalMax"
        Me.AantalMaxDataGridViewTextBoxColumn1.HeaderText = "AantalMax"
        Me.AantalMaxDataGridViewTextBoxColumn1.Name = "AantalMaxDataGridViewTextBoxColumn1"
        '
        'DikteDataGridViewTextBoxColumn1
        '
        Me.DikteDataGridViewTextBoxColumn1.DataPropertyName = "Dikte"
        Me.DikteDataGridViewTextBoxColumn1.HeaderText = "Dikte"
        Me.DikteDataGridViewTextBoxColumn1.Name = "DikteDataGridViewTextBoxColumn1"
        '
        'LokatieDataGridViewTextBoxColumn1
        '
        Me.LokatieDataGridViewTextBoxColumn1.DataPropertyName = "Lokatie"
        Me.LokatieDataGridViewTextBoxColumn1.HeaderText = "Lokatie"
        Me.LokatieDataGridViewTextBoxColumn1.Name = "LokatieDataGridViewTextBoxColumn1"
        '
        'BestandsnaamDataGridViewTextBoxColumn1
        '
        Me.BestandsnaamDataGridViewTextBoxColumn1.DataPropertyName = "Bestandsnaam"
        Me.BestandsnaamDataGridViewTextBoxColumn1.HeaderText = "Bestandsnaam"
        Me.BestandsnaamDataGridViewTextBoxColumn1.Name = "BestandsnaamDataGridViewTextBoxColumn1"
        '
        'WerknummerDataGridViewTextBoxColumn1
        '
        Me.WerknummerDataGridViewTextBoxColumn1.DataPropertyName = "Werknummer"
        Me.WerknummerDataGridViewTextBoxColumn1.HeaderText = "Werknummer"
        Me.WerknummerDataGridViewTextBoxColumn1.Name = "WerknummerDataGridViewTextBoxColumn1"
        '
        'CamLayerDataGridViewTextBoxColumn1
        '
        Me.CamLayerDataGridViewTextBoxColumn1.DataPropertyName = "CamLayer"
        Me.CamLayerDataGridViewTextBoxColumn1.HeaderText = "CamLayer"
        Me.CamLayerDataGridViewTextBoxColumn1.Name = "CamLayerDataGridViewTextBoxColumn1"
        '
        'PonsnibBindingSource
        '
        Me.PonsnibBindingSource.DataMember = "Ponsnib"
        Me.PonsnibBindingSource.DataSource = Me.BrakelSuiteDbDataSet
        '
        'BrakelSuiteDbDataSet
        '
        Me.BrakelSuiteDbDataSet.DataSetName = "BrakelSuiteDbDataSet"
        Me.BrakelSuiteDbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PonsnibTableAdapter
        '
        Me.PonsnibTableAdapter.ClearBeforeFill = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(627, 413)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Clear)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.OpenFiles)
        Me.Controls.Add(Me.AanmakenCSV)
        Me.Controls.Add(Me.CSV)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.LOrdernummer)
        Me.Controls.Add(Me.ListBox1)
        Me.HelpButton = True
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Form1"
        Me.Text = "Generating csv file for Metallix (Nesting)  H. van Boekel  2018"
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PonsnibBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BrakelSuiteDbDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents ListBox1 As ListBox
    Friend WithEvents LOrdernummer As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents CSV As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents AanmakenCSV As Button
    Friend WithEvents OpenFiles As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Clear As Button
    Friend WithEvents OrderDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents IdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents OrdernummerDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MateriaalDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents AantalminDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents AantalMaxDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DikteDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents LokatieDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BestandsnaamDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents WerknummerDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CamLayerDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents BrakelSuiteDbDataSet As BrakelSuiteDbDataSet
    Friend WithEvents PonsnibBindingSource As BindingSource
    Friend WithEvents PonsnibTableAdapter As BrakelSuiteDbDataSetTableAdapters.PonsnibTableAdapter
    Friend WithEvents OrdernummerDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents MateriaalDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents AantalminDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents AantalMaxDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DikteDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents LokatieDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents BestandsnaamDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents WerknummerDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CamLayerDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents BindingSource2 As BindingSource
End Class
