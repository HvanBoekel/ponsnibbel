﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO



Public Class Form1
    Public order As String
    Public Materiaal As String
    Public AantalMin As String
    Public AantalMax As String
    Public Dikte As String
    Public Lokatie As String
    Public Bestandsnaam As String
    Public Werknummer As String
    Public Camlayer As String

    Public fName As String
    Public Map As String
    Public Sfile As String
    Public Sfilenieuw As String
    Public Toevoegfile As Int32
    Public PrNr1 As String
    Public PrNr2 As String
    Public CurrFile As String
    Public WasFile As String
    Public Const ConString = "Data Source=brksq001;Initial Catalog=BrakelsuiteDB;Integrated Security=SSPI;"

    Private Sub subExportDGVToCSV(ByVal strExportFileName As String, ByVal DataGridView As DataGridView, Optional ByVal blnWriteColumnHeaderNames As Boolean = False, Optional ByVal strDelimiterType As String = ",")

        Dim sr As StreamWriter = File.CreateText(strExportFileName)
        Dim strDelimiter As String = strDelimiterType
        Dim intColumnCount As Integer = DataGridView.Columns.Count - 1
        Dim strRowData As String = ""

        If blnWriteColumnHeaderNames Then
            For intX As Integer = 0 To intColumnCount
                strRowData += Replace(Replace(DataGridView.Columns(intX).Name, strDelimiter, ""), "DataGridViewTextBoxColumn1", "") & IIf(intX < intColumnCount, strDelimiter, "")
            Next intX
            sr.WriteLine(strRowData)
        End If

        For intX As Integer = 0 To DataGridView.Rows.Count - 1
            strRowData = ""
            For intRowData As Integer = 0 To intColumnCount
                strRowData += Replace(DataGridView.Rows(intX).Cells(intRowData).Value, strDelimiter, "") & IIf(intRowData < intColumnCount, strDelimiter, "") '''''''''highlights this row
            Next intRowData
            If Len(strRowData) > 15 Then sr.WriteLine(strRowData)
        Next intX
        sr.Close()
    End Sub


    Private Sub DBLeegmaken()
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand

        Try
            con.ConnectionString = ConString
            con.Open()

            cmd.Connection = con
            cmd.CommandText = "Delete from productenlive.ponsnib"


            cmd.ExecuteNonQuery()

        Catch ex As Exception
            MessageBox.Show("Error while inserting record on table..." & ex.Message, "Insert Records")
        Finally
            con.Close()
        End Try

    End Sub

    Private Sub ToevoegenAanSql(Filenaam As String)



        Dim con As New SqlConnection
        Dim cmd As New SqlCommand


        Try
            con.ConnectionString = ConString
            con.Open()

            cmd.Connection = con
            cmd.CommandText = "INSERT INTO productenlive.ponsnib(ordernummer,materiaal,aantalmin,AantalMax,dikte,lokatie,bestandsnaam,werknummer,camlayer) VALUES(@ordernummer, @materiaal,@aantalmin,@Aantalmax,@Dikte,@lokatie,@Bestandsnaam,@Werknummer,@Camlayer)"

            cmd.Parameters.Add("@ordernummer", SqlDbType.VarChar).Value = order
            cmd.Parameters.Add("@materiaal", SqlDbType.VarChar).Value = Materiaal
            cmd.Parameters.Add("@aantalmin", SqlDbType.VarChar).Value = AantalMin
            cmd.Parameters.Add("@aantalmax", SqlDbType.VarChar).Value = AantalMax
            cmd.Parameters.Add("@Dikte", SqlDbType.VarChar).Value = Dikte
            cmd.Parameters.Add("@Lokatie", SqlDbType.VarChar).Value = Lokatie
            cmd.Parameters.Add("@Bestandsnaam", SqlDbType.VarChar).Value = Bestandsnaam
            cmd.Parameters.Add("@Werknummer", SqlDbType.VarChar).Value = Werknummer
            cmd.Parameters.Add("@Camlayer", SqlDbType.VarChar).Value = Camlayer

            cmd.ExecuteNonQuery()

        Catch ex As Exception
            MessageBox.Show("Error while inserting record on table..." & ex.Message, "Insert Records")
        Finally
            con.Close()
        End Try
        DataGridView1.Refresh()
    End Sub


    Private Sub VariabelenToekennen(S As String)
        Dim Tijd As String
        Dim Tel As Integer
        Dim cel As Integer
        Dim filereader As String

        On Error Resume Next

        Sfile = (S)
        Sfilenieuw = Mid(sFile, 1, Len(sFile) - 4) & "_" & CStr(ToevoegFile) & ".dxf"
        ToevoegFile = ToevoegFile + 1


        Map = OpenFileDialog1.FileName
        Map = Mid(Map, 1, Len(Map) - Len(sFile))


        'Name(Map & Sfile As Map & sFileNieuw)
        Sfilenieuw = Sfile
        PrNr1 = Mid(Map, 11, 8)
        PrNr2 = Mid(Map, 20, 3)

        CurrFile = PrNr1 & PrNr2

        If CurrFile <> WasFile Then Werknummer = Werknummer + 1


        order = PrNr1 & "." & PrNr2
        WasFile = CurrFile
        Materiaal = "ALUMINIUM"


        Dim Textline As String
        Dim objReader As New System.IO.StreamReader(S)

        Textline = objReader.ReadLine()
        ' MsgBox(Textline)

        Text -= Textline
        If Textline <> "999" Then
            MsgBox("Let op oude dxf file geen Aantallen en plaatdikte aanwezig")
            AantalMin = "0"
            AantalMax = "0"
            Dikte = "0"
        Else
            Textline = objReader.ReadLine()
            AantalMin = Textline
            AantalMax = Textline
            Textline = objReader.ReadLine()
            Textline = objReader.ReadLine()
            Dikte = Textline
        End If
        Lokatie = Map
        Bestandsnaam = Sfilenieuw
        Werknummer = CStr(Werknummer)
        Camlayer = "4"
    End Sub


    Private Sub OpenFiles_Click(sender As Object, e As EventArgs) Handles OpenFiles.Click
        Dim openFileDialog1 As New OpenFileDialog


        openFileDialog1.InitialDirectory = "c:\temp\dxf"

        openFileDialog1.Filter = "txt files (*.dxf)|*.dxf|All files (*.*)|*.*"

        openFileDialog1.FilterIndex = 1

        openFileDialog1.RestoreDirectory = True

        openFileDialog1.Multiselect = True

        If openFileDialog1.ShowDialog() = DialogResult.OK Then

            For Each fName In openFileDialog1.FileNames

                ListBox1.Items.Add(fName)


                VariabelenToekennen(fName)

                ToevoegenAanSql(fName)

            Next

        End If
        Me.PonsnibTableAdapter.Fill(Me.BrakelSuiteDbDataSet.Ponsnib)
        DataGridView1.Refresh()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        End
    End Sub
    Private Sub ListBox1_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles ListBox1.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.All
        End If
    End Sub


    Private Sub ListBox1_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles ListBox1.DragDrop
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            Dim MyFiles() As String
            Dim i As Integer
            Dim klaar As Boolean


            klaar = False
            ' Assign the files to an array.
            MyFiles = e.Data.GetData(DataFormats.FileDrop)

            ' Loop through the array and add the files to the list.
            For i = 0 To MyFiles.Length - 1
                ' MsgBox(Mid(MyFiles(i), Len(MyFiles(i)) - 3, 5))
                If Mid(MyFiles(i), Len(MyFiles(i)) - 3, 5) = ".dxf" Then
                    ListBox1.Items.Add(MyFiles(i))
                    VariabelenToekennen(MyFiles(i))
                    ToevoegenAanSql(MyFiles(i))
                Else
                    If klaar = False Then MsgBox("Wrong filetype not a dxf file")
                    klaar = True
                End If
            Next
            Me.PonsnibTableAdapter.Fill(Me.BrakelSuiteDbDataSet.Ponsnib)
            DataGridView1.Refresh()
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'BrakelSuiteDbDataSet.Ponsnib' table. You can move, or remove it, as needed.

        DBLeegmaken()
        Me.PonsnibTableAdapter.Fill(Me.BrakelSuiteDbDataSet.Ponsnib)
        Me.DataGridView1.Refresh()
    End Sub

    Private Sub Clear_Click(sender As Object, e As EventArgs) Handles Clear.Click
        ListBox1.Items.Clear()
        DBLeegmaken()
    End Sub


    Private Sub AanmakenCSV_Click(sender As Object, e As EventArgs) Handles AanmakenCSV.Click
        Dim dt As DataTable = New BrakelSuiteDbDataSet.PonsnibDataTable
        Dim Mapnaam As String

        On Error Resume Next
        MkDir("\\data\dxfponsnib\emk\" & CSV.Text)

        Mapnaam = CSV.Text

        subExportDGVToCSV("\\data\dxfponsnib\emk\" & Mapnaam & "\" & Mapnaam & ".csv", DataGridView1, True, ",")

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)

        Dim con As New SqlConnection
        Dim cmd As New SqlCommand

        con.ConnectionString = "Data Source=HANSB\SQLEXPRESS;Initial Catalog=devlocal;Integrated Security=SSPI;"
        con.Open()

        cmd.Connection = con
        cmd.CommandText = "INSERT INTO productenlive.ponsnib(ordernummer,materiaal,aantalmin,AantalMax,dikte,lokatie,bestandsnaam,werknummer,camlayer) VALUES(@ordernummer, @materiaal,@aantalmin,@Aantalmax,@Dikte,@lokatie,@Bestandsnaam,@Werknummer,@Camlayer)"

        Using sda As New SqlDataAdapter()
            cmd.Connection = con
            sda.SelectCommand = cmd
            Using dt As New DataTable()
                sda.Fill(dt)

                'Build the CSV file data as a Comma separated string.
                Dim csv As String = String.Empty

                For Each column As DataColumn In dt.Columns
                    'Add the Header row for CSV file.
                    csv += column.ColumnName + ","c
                Next

                'Add new line.
                csv += vbCr & vbLf

                For Each row As DataRow In dt.Rows
                    For Each column As DataColumn In dt.Columns
                        'Add the Data rows.
                        csv += row(column.ColumnName).ToString().Replace(",", ";") + ","c
                    Next

                    'Add new line.
                    csv += vbCr & vbLf
                Next

                File.WriteAllText("c:\temp\dxf\zalf.csv", csv)

                'Download the CSV file.
            End Using
        End Using
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged

    End Sub
End Class
